package com.qvision.metlife.stepsdefinitions;

import cucumber.api.java.es.Dado;

import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.qvision.metlife.steps.AnalisisDeNecesidadesGspMetLifeSteps;
import com.qvision.metlife.steps.BeneficiariosSolicitudGspMetlifeSteps;
import com.qvision.metlife.steps.CotizacionGspMetLifeSteps;
import com.qvision.metlife.steps.CreacionClienteGspMetLifeSteps;
import com.qvision.metlife.steps.DeclaracionSolicitdGspMetLifeSteps;
import com.qvision.metlife.steps.EstiloVidySalSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.InformacionBasicaSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.InformacionFinancieraSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.LoginGspMetLifeSteps;
import com.qvision.metlife.steps.PagoSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.PresentarSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.ResumenSolicitudGspMetLifeSteps;
import com.qvision.metlife.steps.TomadorPolizaSolicitudGspMetLifeSteps;

import cucumber.api.DataTable;
import cucumber.api.java.es.Y;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.Steps;

public class ExpedicionPolizasGspStepsDefinitions extends PageObject {

	@Steps
	LoginGspMetLifeSteps loginGsp;

	@Steps
	CreacionClienteGspMetLifeSteps nuevoCliente;

	@Steps
	AnalisisDeNecesidadesGspMetLifeSteps analisisDeNecesidades;

	@Steps
	CotizacionGspMetLifeSteps cotizacion;

	@Steps
	InformacionBasicaSolicitudGspMetLifeSteps infoBasica;
	
	@Steps
	TomadorPolizaSolicitudGspMetLifeSteps tomador;
	
	@Steps
	BeneficiariosSolicitudGspMetlifeSteps beneficiario;
	
	@Steps
	EstiloVidySalSolicitudGspMetLifeSteps vidaSalud;
	
	@Steps
	InformacionFinancieraSolicitudGspMetLifeSteps infoFinaciera;
	
	@Steps
	ResumenSolicitudGspMetLifeSteps confirmar;
	
	@Steps
	DeclaracionSolicitdGspMetLifeSteps declaracion;

	@Steps
	PagoSolicitudGspMetLifeSteps pago;
	
	@Steps
	PresentarSolicitudGspMetLifeSteps presentar;
	
	@Dado("^que quiero expedir una nueva p�liza en Gsp$")
	public void queQuieroExpedirUnaNuevaPolizaEnGsp() {
		loginGsp.abrirNavegador();
	}

	@Cuando("^inicie sesi�n en Gsp$")
	public void inicieSesionEnGsp(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		loginGsp.iniciarSesionGsp(listData.get(0).get("usuario"), listData.get(0).get("contrase�a"));

	}

	@Entonces("^valido la sesion en Gsp$")
	public void validoLaSesionEnGsp() {
		loginGsp.validarSesionGsp();
		Assert.assertEquals("Men� Principal", getDriver().findElement(By.id("dashboardTab")).getText());
	}

	@Cuando("^cree un nuevo cliente$")
	public void creeUnNuevoCliente(DataTable arg1) {

		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		nuevoCliente.nuevoCliente(listData.get(0).get("primerNombre"), listData.get(0).get("segundoNombre"),
				listData.get(0).get("tercerNombre"), listData.get(0).get("primerApellido"),
				listData.get(0).get("segundoApellido"), listData.get(0).get("fechaNacimiento"),
				listData.get(0).get("nacionalidad"), listData.get(0).get("genero"),
				listData.get(0).get("clienteFumador"), listData.get(0).get("estadoCivil"),
				listData.get(0).get("ocupacion"), listData.get(0).get("tipoIdentificacion"),
				listData.get(0).get("numeroIdentificacion"), listData.get(0).get("ingresosMensuales"),
				listData.get(0).get("correoElectronico"), listData.get(0).get("direccion"),
				listData.get(0).get("paisResidencia"), listData.get(0).get("departamento"),
				listData.get(0).get("ciudad"), listData.get(0).get("tipoTelefono1"), listData.get(0).get("telefono1"),
				listData.get(0).get("tipoTelefono2"), listData.get(0).get("telefono2"));
	}

	@Entonces("^valido la creaci�n del nuevo cliente$")
	public void validoLaCreacionDelNuevoCliente() {

		nuevoCliente.validacionCreacionCliente();
		Assert.assertEquals("El cliente ha sido salvado exitosamente",
				getDriver().findElement(By.xpath("//*[@id='createNewClientOverlay']/div/div/div[2]/p/span")).getText());

	}

	@Y("^realice el an�lisis de necesidades del cliente$")
	public void realiceElAnalisisDeNecesidadesDelCliente(DataTable arg1) {
		
		 List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		 analisisDeNecesidades.analisDeNecesidades(listData.get(0).get(
		 "salarioDeseado"), listData.get(0).get("ingresoMensual"),
		 listData.get(0).get("semanasCotizadas"), listData.get(0).get("edadPension"));
		

	}

	@Y("^Cree una nueva cotizaci�n$")
	public void creeUnaNuevaCotizaci�n(DataTable arg1) {
		List<Map<String, String>> lisData = arg1.asMaps(String.class, String.class);
		cotizacion.cotizacionGsp(lisData.get(0).get("numeroIdentificacion"), lisData.get(0).get("primaPlan1"),
				lisData.get(0).get("primaPlan2"), lisData.get(0).get("primaPlan3"), lisData.get(0).get("primaPlan4"));
	}

	@Y("^Cree una nueva solicitud$")
	public void creeUnaNuevaSolicitud(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		infoBasica.informacionBasicaSolicitud(listData.get(0).get("paisNacimiento"),
				listData.get(0).get("deptoNacimiento"), listData.get(0).get("ciudadNacimiento"),
				listData.get(0).get("ciudadanoEU"), listData.get(0).get("poderTranferenciaEU"),
				listData.get(0).get("comunicacionMetLife"), listData.get(0).get("infoAdicional1"),
				listData.get(0).get("infoAdicional2"), listData.get(0).get("infoAdicional3"));
	}

	@Y("^Gestione la informaci�n b�sica del tomador de la p�liza$")
	public void gestioneLaInformacionBasicaDelTomadorDeLaPoliza(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		tomador.gestioneInformacionTomadorPoliza(listData.get(0).get("aseguradoIgualTomar"), listData.get(0).get("parentesco"),
				listData.get(0).get("tipoTomador"), listData.get(0).get("primerNombreT"), listData.get(0).get("segundoNombreT"),
				listData.get(0).get("tercerNombreT"), listData.get(0).get("primerApellioT"), listData.get(0).get("segundoApellidoT"), listData.get(0).get("generoT"), 
				listData.get(0).get("fechaNacimientoT"), listData.get(0).get("tipoIdentificacionT"), listData.get(0).get("numeroIdentificacionT"),
				listData.get(0).get("paisNacimiento"), listData.get(0).get("deptoNacimiento"), listData.get(0).get("ciudadNacimiento"),
				listData.get(0).get("nacionalidadT"), listData.get(0).get("estadoCivilT"), listData.get(0).get("direccionResidenciaT"), 
				listData.get(0).get("paisResidenciaT"), listData.get(0).get("deptoResidenciaT"), listData.get(0).get("ciudadResidenciaT"), 
				listData.get(0).get("telefonoResidenciaT"), listData.get(0).get("celularT"), listData.get(0).get("correoElectronicoT"), 
				listData.get(0).get("comunicacionMetL"));
	}

	@Cuando("^Gestione la informaci�n financiera del tomador de la p�liza$")
	public void gestioneLaInformacionFinancieraDelTomadorDeLaPoliza(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		tomador.gestioneInformacionFinancieraT(listData.get(0).get("tipoActividad"), listData.get(0).get("nombreEmpresa"),
				listData.get(0).get("actividadEconomicaEmpT"), listData.get(0).get("nitEmpresaT"), listData.get(0).get("direccionTrabajoT"),
				listData.get(0).get("paisTrabajoT"), listData.get(0).get("deptoTrabajoT"), listData.get(0).get("ciudadTrabajoT"),  
				listData.get(0).get("telefonoEmpreT"), listData.get(0).get("ocupacionT"), listData.get(0).get("actividadEconPrinT"),
				listData.get(0).get("ActiEconSecundariaT"), listData.get(0).get("recursosPubliT"), listData.get(0).get("poderPublicoT"),
				listData.get(0).get("reconocimientoPublicT"), listData.get(0).get("vinculo"), listData.get(0).get("ingresosMensualesT"), 
				listData.get(0).get("otrosIngresosT"), listData.get(0).get("totalActivos"), listData.get(0).get("paisOrigenIngresosT"), 
				listData.get(0).get("egresosMensualesT"), listData.get(0).get("totalPasivosT"), listData.get(0).get("origenFondosT"), 
				listData.get(0).get("transaMonedaExtr"), listData.get(0).get("indemSeguros"));
	}

	@Cuando("^Agregue los beneficiarios$")
	public void agregueLosBeneficiarios(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		beneficiario.agregarBeneficiario(listData.get(0).get("tipoBeneficiario"), listData.get(0).get("tipoIdentificacion"), 
				listData.get(0).get("numDocument"), listData.get(0).get("firstName"), listData.get(0).get("secondName"), 
				listData.get(0).get("threethName"), listData.get(0).get("firstSurname"), listData.get(0).get("secondSurname"), 
				listData.get(0).get("relaWithAse"), listData.get(0).get("fechaNacimientoT"), listData.get(0).get("porcentajeP"), 
				listData.get(0).get("plan"), listData.get(0).get("generoB"), listData.get(1).get("id"));
		
		beneficiario.agregarBeneficiario(listData.get(1).get("tipoBeneficiario"), listData.get(1).get("tipoIdentificacion"), 
				listData.get(1).get("numDocument"), listData.get(1).get("firstName"), listData.get(1).get("secondName"), 
				listData.get(1).get("threethName"), listData.get(1).get("firstSurname"), listData.get(1).get("secondSurname"), 
				listData.get(1).get("relaWithAse"), listData.get(1).get("fechaNacimientoT"), listData.get(1).get("porcentajeP"), 
				listData.get(1).get("plan"), listData.get(1).get("generoB"), listData.get(1).get("id"));
		
	}

	@Cuando("^Gestione el estilo de vida y salud del cliente$")
	public void gestioneElEstiloDeVidaYSaludDelCliente(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		vidaSalud.gestioneInformacionEstiloVida(listData.get(0).get("fuerzaArmada"), listData.get(0).get("practicaDeporte"), 
				listData.get(0).get("viajePiloto"), listData.get(0).get("viajePasajero"),
				listData.get(0).get("vidaF"), listData.get(0).get("pasajero"), listData.get(0).get("consumoDrogas"), 
				listData.get(0).get("fumarAlgo"), listData.get(0).get("beberAlcohol"), listData.get(0).get("procesoJudicial"));
		
		vidaSalud.gestioneInformacionEstadoSalud(listData.get(0).get("embarazo"), listData.get(0).get("dudaEnfermedad"), 
				listData.get(0).get("dolencia"), listData.get(0).get("tratamientos"), listData.get(0).get("rayosX"), 
				listData.get(0).get("enfermedadMas"), listData.get(0).get("herencia"), listData.get(0).get("sida"), 
				listData.get(0).get("sexual"), listData.get(0).get("amputacion"), listData.get(0).get("colesterol"), 
				listData.get(0).get("hipertension"), listData.get(0).get("cancer"), listData.get(0).get("diabetes"), 
				listData.get(0).get("epilepsia"), listData.get(0).get("nariz"), listData.get(0).get("boca"), 
				listData.get(0).get("oidos"), listData.get(0).get("ojos"), listData.get(0).get("vejiga"), 
				listData.get(0).get("depresion"), listData.get(0).get("hepatetica"), listData.get(0).get("columna"), 
				listData.get(0).get("pulmones"), listData.get(0).get("infarto"), listData.get(0).get("sistemaSeguridad"), 
				listData.get(0).get("perdidaPeso"), listData.get(0).get("peso"), listData.get(0).get("estatura"));
	}

	@Cuando("^Agregue la informaci�n financiera del cliente$")
	public void agregueLaInformacionFinancieraDelCliente(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		
		infoFinaciera.gestioneInformacionLaboral(listData.get(0).get("tipoActividad"), listData.get(0).get("nitEmpresaT"),
		listData.get(0).get("nombreEmpresa"), listData.get(0).get("empleadoMetlife"), listData.get(0).get("actividadEconoEmp"), 
		listData.get(0).get("direccionTrabajoT"), listData.get(0).get("paisTrabajoT"), listData.get(0).get("deptoTrabajoT"), 
		listData.get(0).get("ciudadTrabajoT"), listData.get(0).get("telefonoEmpreT"), listData.get(0).get("ocupacionT"), 
		listData.get(0).get("actividadEconPrinT"), listData.get(0).get("ActiEconSecundariaT"), listData.get(0).get("recursosPubliT"), 
		listData.get(0).get("poderPublicoT"), listData.get(0).get("reconocimientoPublicT"), listData.get(0).get("vinculo"));
		
		infoFinaciera.gestioneInformacionFinanciera(listData.get(0).get("ingresosMensualesT"), listData.get(0).get("otrosIngresosT"), 
				listData.get(0).get("paisOrigenIngresosT"), listData.get(0).get("totalActivos"), listData.get(0).get("egresosMensualesT"), 
				listData.get(0).get("totalPasivosT"), listData.get(0).get("origenFondosT"), listData.get(0).get("transaMonedaExtr"), 
				listData.get(0).get("indemSeguros"));
	}

	@Cuando("^Confirme la solicitud$")
	public void confirmeLaSolicitud() {
		confirmar.confirmarSolicitud();	
	}

	@Cuando("^Gestione la declaraci�n del cliente$")
	public void gestioneLaDeclaracionDelCliente() {
		declaracion.gestioneDeclaracion();
	}
	
	@Cuando("^Gestione el pago de la solicitud$")
	public void gestioneElPagoDeLaCotizacion(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		pago.gestionePago(listData.get(0).get("lapsoTiempo"), listData.get(0).get("formaPago"),
				listData.get(0).get("formaPagoRecu"), listData.get(0).get("devolucion"));
	}
	
	@Cuando("^Presente la solicitud$")
	public void presentarSolicitud(DataTable arg1) {
		List<Map<String, String>> listData = arg1.asMaps(String.class, String.class);
		presentar.presentarSolicitud(listData.get(0).get("pathIdenti"),listData.get(0).get("pathCertificado"),
				listData.get(0).get("pathFirma"));	
	}
	
	@Cuando("^Valido la creaci�n de la solicitud$")
	public void validoLaCreacionDeLaSolicitud() {
		
	}

}
