package com.qvision.metlife.pages;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class InformacionFinancieraSolicitudGspMetLifePage extends PageObject{
	
	Esperas espera = new Esperas();
	
	@FindBy(id = "leveltwoListaActivity")
	WebElementFacade sltTipoActividad;
	
	@FindBy(id = "txtFinancialidCompany")
	WebElement txtNitEmp;
	
	@FindBy(id = "dropdownEconomy")
	WebElement txtEconomiaPrin;
	
	@FindBy(id = "economicalActivity")
	WebElement txtEconomiaEmp;
	
	@FindBy(id = "txtFinancialaddressCompany")
	WebElement txtDireccionT;
	
	@FindBy(id = "leveltwoListaPlaces")
	WebElementFacade sltPaisT;
	
	@FindBy(xpath = "//*[@id=\"hidedropActivityOthers\"]/div[3]/div[2]/div[1]/select")
	WebElementFacade sltDepartamentoT;
	
	@FindBy(xpath = "//*[@id=\"hidedropActivityOthers\"]/div[3]/div[2]/div[2]/select")
	WebElementFacade sltCiudadT;
	
	@FindBy(id = "txtFinancialphoneCompany")
	WebElementFacade txtTelefonoT;
	
	@FindBy(id = "selectOccupation0")
	WebElement txtOcupacion;
	
	@FindBy(id = "txtFinancialcompanyName")
	WebElement txtNombreEmp;
	
	@FindBy(id = "yesMetlifeEmployee")
	WebElement rdbEmpleadoMetYes;
	
	@FindBy(id = "noMetlifeEmployee")
	WebElement rdbEmpleadoMetNo;
	
	@FindBy(id = "yesFinanceOne")
	WebElement rdbActEcoSecYes;
	
	@FindBy(id = "noFinanceOne")
	WebElement rdbActEcoSecNo;
	
	@FindBy(id = "yesFinanceTwo")
	WebElement rdbManRecYes;
	
	@FindBy(id = "noFinanceTwo")
	WebElement rdbManRecNo;
	
	@FindBy(id = "yesFinanceThree")
	WebElement rdbEjePodPYes;
	
	@FindBy(id = "noFinanceThree")
	WebElement rdbEjePodPNo;
	
	@FindBy(id = "yesFinanceFour")
	WebElement rdbRecoPubYes;
	
	@FindBy(id = "noFinanceFour")
	WebElement rdbRecoPubNo;
	
	@FindBy(id = "yesFinanceFive")
	WebElement rdbVincPublYes;
	
	@FindBy(id = "noFinanceFive")
	WebElement rdbVincPublNo;
	
	public void gestioneInformacionLaboral(String tipoActividad, String nitEmpresaT, String nombreEmpresa, String empleadoMetlife,
			String actividadEconoEmp, String direccionTrabajoT, String paisTrabajoT, String deptoTrabajoT, String ciudadTrabajoT, 
			String telefonoEmpreT, String ocupacionT, String actividadEconPrinT, String ActiEconSecundariaT, String recursosPubliT, 
			String poderPublicoT, String reconocimientoPublicT, String vinculo) {
		
		try {
			espera.esperaExpEleHabilitado("leveltwoListaActivity", 30);
			sltTipoActividad.selectByVisibleText(tipoActividad);
			txtNitEmp.sendKeys(nitEmpresaT);
			txtNombreEmp.sendKeys(nombreEmpresa);
			
			espera.esperaSimple(1000);
			if(empleadoMetlife.equals("NO"))
				rdbEmpleadoMetNo.click();
			else
				rdbEmpleadoMetYes.click();
			
			txtEconomiaEmp.sendKeys(actividadEconoEmp);
			txtDireccionT.sendKeys(direccionTrabajoT);
			sltPaisT.selectByVisibleText(paisTrabajoT);
			espera.esperaSimple(1000);
			sltDepartamentoT.selectByVisibleText(deptoTrabajoT);
			espera.esperaExpEleHabilitadoByXpath("//*[@id=\"hidedropActivityOthers\"]/div[3]/div[2]/div[2]/select", 10);
			espera.esperaSimple(500);
			sltCiudadT.selectByVisibleText(ciudadTrabajoT);
			txtTelefonoT.sendKeys(telefonoEmpreT);
			txtOcupacion.sendKeys(ocupacionT);
			txtEconomiaPrin.sendKeys(actividadEconPrinT);
			
			espera.esperaSimple(1000);
			if(ActiEconSecundariaT.equals("NO"))
				rdbActEcoSecNo.click();
			else
				rdbActEcoSecYes.click();
			
			espera.esperaSimple(500);
			if(recursosPubliT.equals("NO"))
				rdbManRecNo.click();
			else
				rdbManRecYes.click();
			
			espera.esperaSimple(500);
			if(poderPublicoT.equals("NO"))
				rdbEjePodPNo.click();
			else
				rdbEjePodPYes.click();
			
			espera.esperaSimple(500);
			if(reconocimientoPublicT.equals("NO"))
				rdbRecoPubNo.click();
			else
				rdbRecoPubYes.click();
			
			espera.esperaSimple(500);
			if(vinculo.equals("NO"))
				rdbVincPublNo.click();
			else
				rdbVincPublYes.click();
		}catch(Exception e){
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		
	}
	
	//Informacion Financiera
	
	@FindBy(id = "txtInsFinancialmonthlyIncome")
	WebElement txtIngresosMens;
	
	@FindBy(id = "txtFinancialotherMonthlyIncome")
	WebElement txtOtherIngresos;
	
	@FindBy(id = "incomeCountry")
	WebElementFacade sltPaisOr;
	
	@FindBy(id = "txtassets")
	WebElement txtTotalActivos;
	
	@FindBy(id = "txtmonthlyexpense")
	WebElement txtEgresosM;
	
	@FindBy(id = "txtdebts")
	WebElement txtTotalPasivos;
	
	@FindBy(id = "ListaFundOrigin")
	WebElementFacade sltOriFondAseg;
	
	@FindBy(id = "yesFinanceTransaction")
	WebElement rdbMoneExtrYes;
	
	@FindBy(id = "noFinanceTransaction")
	WebElement rdbMoneExtrno;
	
	@FindBy(id = "yesFinanceTax")
	WebElement rdbImpExtrYes;
	
	@FindBy(id = "noFinanceTax")
	WebElement rdbImpExtrNo;
	
	@FindBy(xpath = "//*[@id=\"application_financialDetails_formFooter\"]/div/button[3]")
	WebElement btnContinuar;

	public void gestioneInformacionFinanciera(String ingresosMensualesT, String otrosIngresosT, 
			String paisOrigenIngresosT, String totalActivos, String egresosMensualesT, String totalPasivosT,
			String origenFondosT, String transaMonedaExtr, String indemSeguros) {
		
		txtIngresosMens.sendKeys(ingresosMensualesT);
		txtOtherIngresos.sendKeys(otrosIngresosT);
		sltPaisOr.selectByVisibleText(paisOrigenIngresosT);
		txtTotalActivos.sendKeys(totalActivos);
		txtEgresosM.sendKeys(egresosMensualesT);
		txtTotalPasivos.sendKeys(totalPasivosT);
		sltOriFondAseg.selectByVisibleText(origenFondosT);
		
		if(transaMonedaExtr.equals("NO"))
			rdbMoneExtrno.click();
		else
			rdbMoneExtrYes.click();
		
		if(indemSeguros.equals("NO"))
			rdbImpExtrNo.click();
		else
			rdbImpExtrYes.click();
		
		espera.esperaSimple(3000);
		btnContinuar.click();
	}
}