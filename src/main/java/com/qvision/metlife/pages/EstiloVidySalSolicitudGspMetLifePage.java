package com.qvision.metlife.pages;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class EstiloVidySalSolicitudGspMetLifePage extends PageObject{
	
	Esperas esperas = new Esperas();

	// Informacion Estilo de vida

	@FindBy(xpath = "//*[@id=\"noTsix\"]")
	WebElement rdbFuerzaANo;

	@FindBy(xpath = "//*[@id=\\\"yesTsix\\\"]")
	WebElement rdbFuerzaAYes;

	@FindBy(id = "yesTseven")
	WebElement rdbPracticaYes;

	@FindBy(id = "noTseven")
	WebElement rdbPracticaNo;

	@FindBy(id = "yesTeight")
	WebElement rdbViajePasajeroYes;

	@FindBy(id = "noTeight")
	WebElement rdbViajePasajeroNo;
	
	@FindBy(id = "yesTnine")
	WebElement rdbViajePilotoYes;

	@FindBy(id = "noTnine")
	WebElement rdbViajePilotoNo;

	@FindBy(id = "noThirty")
	WebElement rdbVidaFNo;

	@FindBy(id = "yesThirty")
	WebElement rdbVidaFYes;

	@FindBy(id = "noThirtyone")
	WebElement rdbPasajeroNo;

	@FindBy(id = "yesThirtyone")
	WebElement rdbPasajeroYes;

	@FindBy(id = "noThirtytwo")
	WebElement rdbConsumoDNo;

	@FindBy(id = "yesThirtytwo")
	WebElement rdbConsumoDYes;

	@FindBy(id = "noThirtythree")
	WebElement rdbFumadoNo;

	@FindBy(id = "yesThirtythree")
	WebElement rdbFumadoYes;

	@FindBy(id = "noThirtyfour")
	WebElement rdbAlcoholNo;

	@FindBy(id = "yesThirtyfour")
	WebElement rdbAlcoholYes;

	@FindBy(id = "noThirtyfive")
	WebElement rdbProcesoJudicialNo;

	@FindBy(id = "yesThirtyfive")
	WebElement rdbProcesoJudicialYes;

	public void gestioneInformacionEstiloVida(String fuerzaArmada, String practicaDeporte, String viajePiloto, String viajePasajero,
			String vidaF, String pasajero, String consumoDrogas, String fumarAlgo, String beberAlcohol, String procesoJudicial) {
	
		esperas.esperaSimple(3000);
		if (fuerzaArmada.equals("NO")) {
			rdbFuerzaANo.click();
		} else {
			rdbFuerzaAYes.click();
		}
		
		if (practicaDeporte.equals("NO")) {
			rdbPracticaNo.click();
		} else {
			rdbPracticaYes.click();
		}
		
		if (viajePiloto.equals("NO")) {
			rdbViajePilotoNo.click();
		} else {
			rdbViajePilotoYes.click();
		}

		if (viajePasajero.equals("NO")) {
			rdbViajePasajeroNo.click();
		} else {
			rdbViajePasajeroYes.click();
		}

		if (vidaF.equals("NO")) {
			rdbVidaFNo.click();
		} else {
			rdbVidaFYes.click();
		}

		if (pasajero.equals("NO")) {
			rdbPasajeroNo.click();
		} else {
			rdbPasajeroYes.click();
		}

		if (consumoDrogas.equals("NO")) {
			rdbConsumoDNo.click();
		} else {
			rdbConsumoDYes.click();
		}

		if (fumarAlgo.equals("NO")) {
			rdbFumadoNo.click();
		} else {
			rdbFumadoYes.click();
		}

		if (beberAlcohol.equals("NO")) {
			rdbAlcoholNo.click();
		} else {
			rdbAlcoholYes.click();
		}

		if (procesoJudicial.equals("NO")) {
			rdbProcesoJudicialNo.click();
		} else {
			rdbProcesoJudicialYes.click();
		}

	}


	 //Informacion estado Salud
	
	  
	 @FindBy(id = "txtLifestyleHeight") WebElement txtEstatura;
	  
	 @FindBy(id = "txtLifestyleWeight") WebElement txtPeso;
	  
	 @FindBy(id = "noOne") WebElement rdbPerdidaPesoNo;
	  
	 @FindBy(id = "yesOne") WebElement rdbPerdidaPesoYes;
	  
	 @FindBy(id = "noTwo") WebElement rdbSistemaSNo;
	  
	 @FindBy(id = "yesTwo") WebElement rdbSistemaSYes;
	  
	 @FindBy(id = "noThree") WebElement rdbInfartoNo;
	  
	 @FindBy(id = "yesThree") WebElement rdbInfartoYes;
	  
	 @FindBy(id = "noFour") WebElement rdbPulmonesNo;
	  
	 @FindBy(id = "yesFour") WebElement rdbPulmonesYes;
	  
	 @FindBy(id = "yesFive") WebElement rdbColumnaYes;
	  
	 @FindBy(id = "noFive") WebElement rdbColumnaNo;
	  
	 @FindBy(id = "noSix") WebElement rdbHepaticaNo;
	  
	 @FindBy(id = "yesSix") WebElement rdbHepaticaYes;
	 
	 @FindBy(id = "yesSeven") WebElement rdbDepresionYes;
	  
	 @FindBy(id = "noSeven") WebElement rdbDepresionNo;
	  
	 @FindBy(id = "yesEight") WebElement rdbVejigaYes;
	  
	 @FindBy(id = "noEight") WebElement rdbVejigaNo;
	  
	 @FindBy(id = "yesNine") WebElement rdbOjosYes;
	  
	 @FindBy(id = "noNine") WebElement rdbOjosNo;
	  
	 @FindBy(id = "yesTen") WebElement rdbOidosYes;
	  
	 @FindBy(id = "noTen") WebElement rdbOidosNo;
	 
	 @FindBy(id = "yesEleven") WebElement rdbBocaYes;
	  
	 @FindBy(id = "noEleven") WebElement rdbBocaNo;
	  
	 @FindBy(id = "yesTwelve") WebElement rdbNarizYes;
	  
	 @FindBy(id = "noTwelve") WebElement rdbNarizNo;
	  
	 @FindBy(id = "yesThirten") WebElement rdbEpilepsiaYes;
	  
	 @FindBy(id = "noThirten") WebElement rdbEpilepsiaNo;
	  
	 @FindBy(id = "yesFourten") WebElement rdbDiabetesYes;
	  
	 @FindBy(id = "noFourten") WebElement rdbDiabetesNo;
	  
	 @FindBy(id = "yesFiften") WebElement rdbCancerYes;
	  
	 @FindBy(id = "noFiften") WebElement rdbCancerNo;
	  
	 @FindBy(id = "yesSixten") WebElement rdbHipertensionYes;
	  
	 @FindBy(id = "noSixten") WebElement rdbHipertensionNo;
	  
	 @FindBy(id = "yesSeventen") WebElement rdbColesterolYes;
	  
	 @FindBy(id = "noSeventen") WebElement rdbColesterolNo;
	  
	 @FindBy(id = "yesEighten") WebElement rdbAmputacionYes;
	  
	 @FindBy(id = "noEighten") WebElement rdbAmputacionNo;
	  
	 @FindBy(id = "yesNineten") WebElement rdbSexualYes;
	  
	 @FindBy(id = "noNineten") WebElement rdbSexualNo;
	  
	 @FindBy(id = "yesTwenty") WebElement rdbSidaYes;
	  
	 @FindBy(id = "noTwenty") WebElement rdbSidaNo;
	  
	 @FindBy(id = "yesTone") WebElement rdbHerenciaGeneticaYes;
	  
	 @FindBy(id = "noTone") WebElement rdbHerenciaGeneticaNo;
	  
	 @FindBy(id = "yesThirtySix") WebElement rdbEnfermedadMasYes;
	  
	 @FindBy(id = "noThirtySix") WebElement rdbEnfermedadMasNo;
	  
	 @FindBy(id = "yesThirtySeven") WebElement rdbRayosXYes;
	  
	 @FindBy(id = "noThirtySeven") WebElement rdbRayosXNo;
	 
	 @FindBy(id = "yesTtwo") WebElement rdbTratamientoYes;
	  
	 @FindBy(id = "noTtwo") WebElement rdbTratamientoNo;
	  
	 @FindBy(id = "yesTthree") WebElement rdbDolenciaYes;
	  
	 @FindBy(id = "noTthree") WebElement rdbDolenciaNo;
	  
	 @FindBy(id = "yesTfour") WebElement rdbDudaSaludYes;
	  
	 @FindBy(id = "noTfour") WebElement rdbDudaSaludNo;
	  
	 @FindBy(id = "yesTfive") WebElement rdbEmbarazadaYes;
	 
	 @FindBy(id = "noTfive") WebElement rdbEmbarazadaNo;
	 
	 @FindBy(xpath = "//*[@id=\"application_lifestyleDetails_formFooter\"]/div/button[3]") WebElement btnContinuar;
	 
	public void gestioneInformacionEstadoSalud(String embarazo, String dudaEnfermedad, String dolencia, 
			String tratamientos, String rayosX, String enfermedadMas, String herencia, String sida, 
			String sexual, String amputacion, String colesterol, String hipertension, String cancer, 
			String diabetes, String epilepsia, String nariz, String boca, String oidos, String ojos, 
			String vejiga, String depresion, String hepatetica, String columna, String pulmones, 
			String infarto, String sistemaSeguridad, String perdidaPeso, String peso, String estatura) {
		
		esperas.esperaSimple(1000);		
		txtEstatura.sendKeys(estatura);
		txtPeso.sendKeys(peso);
		
		if(perdidaPeso.equals("NO")) {
			rdbPerdidaPesoNo.click();
		}else {
			rdbPerdidaPesoYes.click();
		}
		
		if(sistemaSeguridad.equals("NO")) {
			rdbSistemaSNo.click();
		}else {
			rdbSistemaSYes.click();
		
		}
		
		if(infarto.equals("NO")) {
			rdbInfartoNo.click();
		}else {
			rdbInfartoYes.click();
		
		}
		
		if(pulmones.equals("NO")) {
			rdbPulmonesNo.click();
		}else {
			rdbPulmonesYes.click();
		}
		
		if(columna.equals("NO")) {
			rdbColumnaNo.click();
		}else {
			rdbColumnaYes.click();
		
		}
		
		if(hepatetica.equals("NO")) {
			rdbHepaticaNo.click();
		}else {
			rdbHepaticaYes.click();
		
		}
		
		if(depresion.equals("NO")) {
			rdbDepresionNo.click();
		}else {
			rdbDepresionYes.click();
		
		}
		
		if(vejiga.equals("NO")) {
			rdbVejigaNo.click();
		}else {
			rdbVejigaYes.click();
		
		}
		
		if(ojos.equals("NO")) {
			rdbOjosNo.click();
		}else {
			rdbOjosYes.click();
		
		}
		
		if(oidos.equals("NO")) {
			rdbOidosNo.click();
		}else {
			rdbOidosYes.click();
		
		}
		
		if(boca.equals("NO")) {
			rdbBocaNo.click();
		}else {
			rdbBocaYes.click();
		
		}
		
		if(nariz.equals("NO")) {
			rdbNarizNo.click();
		}else {
			rdbNarizYes.click();
		
		}
		
		if(epilepsia.equals("NO")) {
			rdbEpilepsiaNo.click();
		}else {
			rdbEpilepsiaYes.click();
		
		}
		
		if(diabetes.equals("NO")) {
			rdbDiabetesNo.click();
		}else {
			rdbDiabetesYes.click();
		
		}
		
		if(cancer.equals("NO")) {
			rdbCancerNo.click();
		}else {
			rdbCancerNo.click();
		}
		
		if(hipertension.equals("NO")) {
			rdbHipertensionNo.click();
		}else {
			rdbHipertensionYes.click();
		
		}
		
		if(colesterol.equals("NO")) {
			rdbColesterolNo.click();
		}else {
			rdbColesterolYes.click();
		
		}
		
		if(amputacion.equals("NO")) {
			rdbAmputacionNo.click();
		}else {
			rdbAmputacionYes.click();
		
		}
		
		if(sexual.equals("NO")) {
			rdbSexualNo.click();
		}else {
			rdbSexualYes.click();
		
		}
		
		if(sida.equals("NO")) {
			rdbSidaNo.click();
		}else {
			rdbSidaYes.click();
		
		}
		
		if(herencia.equals("NO")) {
			rdbHerenciaGeneticaNo.click();
		}else {
			rdbHerenciaGeneticaYes.click();
		
		}
		
		if(enfermedadMas.equals("NO")) {
			rdbEnfermedadMasNo.click();
		}else {
			rdbEnfermedadMasYes.click();
		
		}
		
		if(rayosX.equals("NO")) {
			rdbRayosXNo.click();
		}else {
			rdbRayosXYes.click();
		
		}
		
		if(tratamientos.equals("NO")) {
			rdbTratamientoNo.click();
		}else {
			rdbTratamientoYes.click();
		
		}
		
		if(dolencia.equals("NO")) {
			rdbDolenciaNo.click();
		}else {
			rdbDolenciaYes.click();
		
		}
		
		if(dudaEnfermedad.equals("NO")) {
			rdbDudaSaludNo.click();
		}else {
			rdbDudaSaludYes.click();
		
		}
		
		if(embarazo.equals("NO")) {
			rdbEmbarazadaNo.click();
		}else {
			rdbEmbarazadaYes.click();
		}
		
		esperas.esperaSimple(3000);
		btnContinuar.click();
	}	
}
