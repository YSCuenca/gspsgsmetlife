package com.qvision.metlife.pages;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class ResumenSolicitudGspMetLifePage extends PageObject {

	Esperas espera = new Esperas();

	@FindBy(id = "btnSummaryContinue")
	WebElement btnContinuar;

	@FindBy(xpath = "//*[@id=\"appSummeryInsuredDetails\"]/div/div[1]/h3")
	WebElement title;

	public void confirmarSolicitud() {

		espera.esperaExpEleHabilitado("btnSummaryContinue", 30);
		btnContinuar.click();

	}

}
