package com.qvision.metlife.pages;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://qa.salespilot.metlife.com.co/")
public class LoginGspMetLifePage extends PageObject {
	Esperas esperaExpl;

	@FindBy(name = "USER")
	WebElement txtusuario;

	@FindBy(name = "PASSWORD")
	WebElement txtcontrasena;

	@FindBy(id = "btnSubmit")
	WebElement btnIngresar;

	public void iniciarSesionGsp(String usuario, String contraseņa) {
		esperaExpl.esperaSimple(5000);
		txtusuario.sendKeys(usuario);
		txtcontrasena.sendKeys(contraseņa);
	}

	public void validarSesionGsp() {
		btnIngresar.click();
		esperaExpl.esperaExpEleVisible("dashboardTab", 8);
	}

}
