package com.qvision.metlife.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class TomadorPolizaInfoBasicaSolicitudGspMetLifePage extends PageObject {

	Esperas espera = new Esperas();
	
	@FindBy(id = "sameAsInsuredYes")
	WebElement aseguradoIgualTomadorSi;
	
	@FindBy(id = "sameAsInsuredNo")
	WebElement aseguradoIgualTomadorNo;

	@FindBy(id = "ownerRelationType")
	WebElementFacade sltParenteco;

	@FindBy(id = "policyHolderType")
	WebElementFacade sltTipoTomador;

	@FindBy(id = "txtOwnerFirstName")
	WebElement txtPrimerNombreT;

	@FindBy(id = "txtOwnerSecondName")
	WebElement txtSegundoNombreT;

	@FindBy(id = "txtOwnerSecondMiddleName")
	WebElement txtTercerNombreT;

	@FindBy(id = "txtOwnerLastName")
	WebElement txtPrimerApellioT;

	@FindBy(id = "txtOwnerSecondSurname")
	WebElement txtSegundoApellidoT;

	@FindBy(id = "male")
	WebElement rdbGeneroM;

	@FindBy(id = "female")
	WebElement rdbGeneroF;

	@FindBy(id = "txtOwnerDateOfBirth")
	WebElement txtFechaNacimientoT;

	@FindBy(id = "ownDocumentType")
	WebElementFacade sltTipoIdentificacionT;

	@FindBy(id = "ownerDocumentNumber")
	WebElement txtIdentificacionT;

	@FindBy(id = "countryBirth")
	WebElementFacade sltPaisNacimientoT;

	@FindBy(id = "txtOwnerState")
	WebElementFacade sltDeptoNacimientoT;

	@FindBy(id = "txtOwnerCity")
	WebElementFacade sltCiudadNacimientoT;

	@FindBy(id = "selectNation0")
	WebElementFacade sltNacionalidadT;

	@FindBy(id = "selectMaritalStatus")
	WebElementFacade sltEstadoCivilT;

	@FindBy(id = "txtAddress")
	WebElement txtDireccionResideT;

	@FindBy(id = "homeCountry")
	WebElementFacade sltPaisResidenciaT;

	@FindBy(id = "txtOwnerHomeState")
	WebElementFacade sltDeptoResidenciaT;

	@FindBy(id = "txtOwnerHomeCity")
	WebElementFacade sltCiudadResidenciaT;

	@FindBy(id = "txtHomeNumber")
	WebElement txtTelefonoResideT;

	@FindBy(id = "txtMobileNumber")
	WebElement txtCelularT;

	@FindBy(id = "txtOwnerEmailAddr")
	WebElement txtCorreoElectronicoT;

	@FindBy(id = "homeAddrYes")
	WebElement rdbComunicacionSi;

	@FindBy(id = "homeAddrNo")
	WebElement rdbComunicacionNo;

	@FindBy(id = "selectActivityType")
	WebElementFacade sltTipoActividadT;

	@FindBy(id = "personCompanyname")
	WebElement txtNombreEmpresaT;

	@FindBy(id = "selectEconomicActivity")
	WebElementFacade sltActiviaEconomicaEmpreT;

	@FindBy(id = "personNITCompany")
	WebElement txtNitEmpresaT;

	@FindBy(id = "personAddressCompany")
	WebElement txtDireccionTrabajoT;

	@FindBy(id = "selectCountryPolicyHolder")
	WebElementFacade sltPaisTrabajoT;

	@FindBy(id = "companyMailingState")
	WebElementFacade sltDeptoTrabajoT;

	@FindBy(id = "companyMailingCity")
	WebElementFacade sltCiudadTrabajoT;

	@FindBy(id = "companyPhoneNumberPerson")
	WebElement txtTelefonoEmpresaT;

	@FindBy(id = "selectOccup0")
	WebElement txtOcupacionT;

	@FindBy(id = "personPrincipleEconomy")
	WebElementFacade sltActEconoPrincipalT;

	@FindBy(id = "personPlaceHoderQuestionOneYes")
	WebElement rdbActEcSecundariaSi;

	@FindBy(id = "personPlaceHoderQuestionOneNo")
	WebElement rdbActEcSecundariaNo;

	@FindBy(id = "personPlaceHoderQuestionTwoYes")
	WebElement rdbRecursosPublicosSi;

	@FindBy(id = "personPlaceHoderQuestionTwoNo")
	WebElement rdbRecursosPublicosNo;

	@FindBy(id = "personPlaceHoderQuestionThreeYes")
	WebElement rdbPoderPublicoSi;

	@FindBy(id = "personPlaceHoderQuestionThreeNo")
	WebElement rdbPoderPublicoNo;

	@FindBy(id = "personPlaceHoderQuestionFourYes")
	WebElement rdbReconocPublicoSi;

	@FindBy(id = "personPlaceHoderQuestionFourNo")
	WebElement rdbReconocPublicoNo;

	@FindBy(id = "personPlaceHoderQuestionFiveYes")
	WebElement rdbVinculoSi;

	@FindBy(id = "personPlaceHoderQuestionFiveNo")
	WebElement rdbVinculoNo;

	@FindBy(id = "txtFinancialmonthlyIncome")
	WebElement txtIngresosT;

	@FindBy(id = "txtFinancialotherMonthlyIncome")
	WebElement txtOtrosIngresosT;

	@FindBy(id = "leveltwoListaFundOrigin")
	WebElementFacade sltConceptoOtrosIngresosT;

	@FindBy(id = "txtOwnassets")
	WebElement txtTotalActivosT;

	@FindBy(id = "incomeCountry")
	WebElementFacade sltPaisOrigenIngresosT;

	@FindBy(id = "txtmonthlyexpense")
	WebElement txtEgresosT;

	@FindBy(id = "txtdebts")
	WebElement txtTotalPasivosT;

	@FindBy(id = "ListaFundOrigin")
	WebElementFacade sltOrigenFondosT;

	@FindBy(id = "transactionYes")
	WebElement rdbTranMonedaExtSi;

	@FindBy(id = "transactionNo")
	WebElement rdbTranMonedaExtNo;

	@FindBy(id = "policyYes")
	WebElement rdbIndemnisacionSi;

	@FindBy(id = "policyNo")
	WebElement rdbIndemnisacionNo;

	@FindBy(id = "btnOwnerContinue")
	WebElement btnContinuarTomador;


	public void gestioneInformacionTomadorPoliza(String aseguradoIgualTomar, String parentesco, String tipoTomador,
			String primerNombreT, String segundoNombreT, String tercerNombreT, String primerApellioT,
			String segundoApellidoT, String generoT, String fechaNacimientoT, String tipoIdentificacionT,
			String numeroIdentificacionT, String paisNacimiento, String deptoNacimiento, String ciudadNacimiento,
			String nacionalidadT, String estadoCivilT, String direccionResidenciaT, String paisResidenciaT,
			String deptoResidenciaT, String ciudadResidenciaT, String telefonoResidenciaT, String celularT,
			String correoElectronicoT, String comunicacionMetL) {

		if (aseguradoIgualTomar.equals("NO")) {
			espera.esperaSimple(3000);
			espera.esperaExpEleHabilitado("sameAsInsuredNo", 5);
			aseguradoIgualTomadorNo.click();
		} else {
			espera.esperaSimple(3000);
			espera.esperaExpEleHabilitado("sameAsInsuredYes", 5);
			aseguradoIgualTomadorSi.click();
			
			espera.esperaSimple(4000);
			
			espera.esperaExpEleHabilitado("ownerRelationType", 10);
			sltParenteco.selectByVisibleText(parentesco);
			sltTipoTomador.selectByVisibleText(tipoTomador);
			espera.esperaSimple(1000);
			espera.esperaExpEleHabilitado("txtOwnerFirstName", 10);
			txtPrimerNombreT.sendKeys(primerNombreT);
			txtSegundoNombreT.sendKeys(segundoNombreT);
			txtPrimerApellioT.sendKeys(primerApellioT);
			txtSegundoApellidoT.sendKeys(segundoApellidoT);
			txtTercerNombreT.sendKeys(tercerNombreT);

			if (generoT.equals("M")) {
				rdbGeneroM.click();
			} else {
				rdbGeneroM.click();
			}
			
			JavascriptExecutor js = (JavascriptExecutor) getDriver();  
			js.executeScript("document.getElementById('txtOwnerDateOfBirth').value='"+fechaNacimientoT+"'");
			
			sltTipoIdentificacionT.selectByVisibleText(tipoIdentificacionT);
			espera.esperaSimple(1000);
			espera.esperaExpEleHabilitado("ownerDocumentNumber", 10);
			txtIdentificacionT.sendKeys(numeroIdentificacionT);
			sltPaisNacimientoT.selectByVisibleText(paisNacimiento);
			espera.esperaSimple(1000);
			espera.esperaExpEleHabilitado("txtOwnerState", 10);
			sltDeptoNacimientoT.selectByVisibleText(deptoNacimiento);
			espera.esperaSimple(1000);
			espera.esperaExpEleHabilitado("txtOwnerCity", 10);
			sltCiudadNacimientoT.selectByVisibleText(ciudadNacimiento);
			espera.esperaSimple(1000);
			sltNacionalidadT.selectByVisibleText(nacionalidadT);
			sltEstadoCivilT.selectByVisibleText(estadoCivilT);
			txtDireccionResideT.sendKeys(direccionResidenciaT);
			sltPaisResidenciaT.selectByVisibleText(paisResidenciaT);
			espera.esperaSimple(1000);
			sltDeptoResidenciaT.selectByVisibleText(deptoResidenciaT);
			espera.esperaSimple(1000);
			sltCiudadResidenciaT.selectByVisibleText(ciudadResidenciaT);
			espera.esperaSimple(1000);
			txtTelefonoResideT.sendKeys(telefonoResidenciaT);
			txtCelularT.sendKeys(celularT);
			txtCorreoElectronicoT.sendKeys(correoElectronicoT);

			if (comunicacionMetL.equals("SI")) {
				rdbComunicacionSi.click();
			} else {
				rdbComunicacionNo.click();
			}

		}

	}

	public void gestioneInformacionFinancieraT(String tipoActividad, String nombreEmpresa, String actividadEconomicaEmpT, String nitEmpresaT, String direccionTrabajoT, 
			String paisTrabajoT, String deptoTrabajoT, String ciudadTrabajoT, String telefonoEmpreT, String ocupacionT, String actividadEconPrinT, 
			String ActiEconSecundariaT, String recursosPubliT, String poderPublicoT, String reconocimientoPublicT, String vinculo, String ingresosMensualesT, 
			String otrosIngresosT, String totalActivos, String paisOrigenIngresosT, String egresosMensualesT, String totalPasivosT, String origenFondosT, 
			String transaMonedaExtr, String indemSeguros){
		
		
		sltTipoActividadT.selectByVisibleText(tipoActividad);
		espera.esperaSimple(1000);
		espera.esperaExpEleHabilitado("personCompanyname", 10);
		txtNombreEmpresaT.sendKeys(nombreEmpresa);
		sltActiviaEconomicaEmpreT.sendKeys(actividadEconomicaEmpT);
		txtNitEmpresaT.sendKeys(nitEmpresaT);
		txtDireccionTrabajoT.sendKeys(direccionTrabajoT);
		sltPaisTrabajoT.selectByVisibleText(paisTrabajoT);
		espera.esperaSimple(1000);
		espera.esperaExpEleHabilitado("personCompanyname", 10);
		sltDeptoTrabajoT.selectByVisibleText(deptoTrabajoT);
		espera.esperaSimple(1000);
		espera.esperaExpEleHabilitado("companyMailingState", 10);
		sltCiudadTrabajoT.selectByVisibleText(ciudadTrabajoT);
		espera.esperaSimple(1000);
		espera.esperaExpEleHabilitado("companyPhoneNumberPerson", 10);
		txtTelefonoEmpresaT.sendKeys(telefonoEmpreT);
		txtOcupacionT.sendKeys(ocupacionT);
		sltActEconoPrincipalT.selectByVisibleText(actividadEconPrinT);
		
		if(ActiEconSecundariaT.equals("NO")) {
			rdbActEcSecundariaNo.click();
		}else {
			rdbActEcSecundariaSi.click();
		}
		if(recursosPubliT.equals("NO")) {
			rdbRecursosPublicosNo.click();
		}else {
			rdbRecursosPublicosSi.click();
		}
		if(poderPublicoT.equals("NO")) {
			rdbPoderPublicoNo.click();
		}else {
			rdbPoderPublicoSi.click();
		}
		if(reconocimientoPublicT.equals("NO")) {
			rdbReconocPublicoNo.click();
		}else {
			rdbReconocPublicoSi.click();
		}
		if(vinculo.equals("NO")) {
			rdbVinculoNo.click();
		}else {
			rdbVinculoSi.click();
		}
		
		txtIngresosT.sendKeys(ingresosMensualesT);
		txtOtrosIngresosT.sendKeys(otrosIngresosT);
		txtTotalActivosT.sendKeys(totalActivos);
		sltPaisOrigenIngresosT.selectByVisibleText(paisOrigenIngresosT);
		espera.esperaSimple(1000);
		txtEgresosT.sendKeys(egresosMensualesT);
		espera.esperaSimple(1000);
		txtTotalPasivosT.sendKeys(totalPasivosT);
		
		sltOrigenFondosT.selectByVisibleText(origenFondosT);
		
		if(transaMonedaExtr.equals("NO")) {
			rdbTranMonedaExtNo.click();
		}else {
			rdbTranMonedaExtSi.click();
		}
		
		if(indemSeguros.equals("NO")) {
			rdbIndemnisacionNo.click();
		}else {
			rdbIndemnisacionSi.click();
			
		}
		btnContinuarTomador.click();
		
		espera.esperaExpEleVisibleByXpath("//*[@id=\"addBeneficiary\"]/button[2]", 30);
		espera.esperaExpEleHabilitadoByXpath("//*[@id=\"addBeneficiary\"]/button[2]", 30);
	}
}
