package com.qvision.metlife.pages;

import java.util.List;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class CotizacionGspMetLifePage extends PageObject {

	// Instanciacion de clases
	Esperas espera = new Esperas();
	PlanesCotizacionGspMetLigePage planesCotizacion = new PlanesCotizacionGspMetLigePage();

	// Creacion de variables, obtencion de elementos (WebElements, etc) necesarios
	// para la automatizacion
	@FindBy(id = "illustrationTab")
	WebElement lnkcotizacion;

	@FindBy(id = "btnNewIllustartion")
	WebElement btnNuevaCotizacion;

	@FindBy(id = "advSearchLink")
	WebElement lnkBusquedaAvanzada;

	@FindBy(id = "DocIdNumber")
	WebElement txtNumDoc;

	@FindBy(id = "SearchButtonAdv")
	WebElement btnBuscar;

	@FindBy(xpath = "//*[@id=\"searchResults\"]/div/div/div/div/div/div/div/div/div/div[1]/div/div[8]/div/button[2]")
	WebElement btnOpciones;

	@FindBy(id = "newIllustrationLink")
	WebElement btnNuevaCotizacion2;

	@FindBy(id = "faceAmountInput_VT6")
	WebElement txtValorAseguradoVE60;

	@FindBy(id = "checkBoxInput_VT61")
	WebElement txtAmparosVE60;

	@FindBy(id = "faceAmountInput_80P")
	WebElement txtValorAseguradoTPPlus;

	@FindBy(id = "checkBoxInput_80P0")
	WebElement txtAmparosTPPlus;

	@FindBy(id = "planSubmitVT6")
	WebElement btnCalcularVE60;

	@FindBy(id = "planSubmit80P")
	WebElement btnCalcularTPPlus;

	@FindBy(id = "btnReviewQuote")
	WebElement btnRevisarCotizacion;
	// ---------------------------------------------------------------------------------------------------
	@FindBy(xpath = "//*[@id=\"productFamily\"]/div[1]/span")
	List<WebElement> lnkPlanes;

	@FindBy(id = "btnStartQuote")
	WebElement btnCotizar;

	@FindBy(id = "endIllustration")
	WebElement btnConfirmarVenta;

	@FindBy(id = "btnMarkCompleteYes")
	WebElement btnSiConfirmarVenta;

	// Metodos

	// Proceso para realizar una cotizacion
	public void cotizacion(String numeroIdentificacion, String primaPlan1, String primaPlan2, String primaPlan3,
			String primaPlan4) {

		//
		lnkcotizacion.click();
		espera.esperaExpEleVisible("illustrationSearch", 3);

		btnNuevaCotizacion.click();
		espera.esperaExpEleHabilitado("clientSearchTextBox", 3);

		lnkBusquedaAvanzada.click();
		txtNumDoc.sendKeys(numeroIdentificacion);
		btnBuscar.click();

		espera.esperaExpEleHabilitadoByXpath("//*[@id=\"searchResults\"]/div/div/div/div/div/div/div/div/div/div[1]/div/div[8]/div/button[2]", 4);
		espera.esperaSimple(10000);
		btnOpciones.click();
		espera.esperaSimple(1000);

		btnNuevaCotizacion2.click();
		espera.esperaExpEleVisible("btnStartQuote", 8);

		lnkPlanes.get(0).click();
		planesCotizacion.agregarPlanVidaEntera50();
		espera.esperaSimple(1000);

		lnkPlanes.get(2).click();
		espera.esperaSimple(6000);

		btnCotizar.click();
		espera.esperaExpEleHabilitado("faceAmountInput_VT6", 5);

		planesCotizacion.agregarValorPrima("faceAmountInput_VT6", primaPlan1);
		planesCotizacion.agregarValorPrima("checkBoxInput_VT61", primaPlan2);
		espera.esperaSimple(1000);
		planesCotizacion.clickButton("planSubmitVT6");
		espera.esperaExpEleHabilitado("btnReviewQuote", 5);
		btnRevisarCotizacion.click();

		espera.esperaExpEleHabilitado("endIllustration", 5);
		btnConfirmarVenta.click();
		espera.esperaExpEleVisible("btnMarkCompleteYes", 10);
		btnSiConfirmarVenta.click();
		espera.esperaExpEleVisible("txtAppSecondName", 30);
	}

}
