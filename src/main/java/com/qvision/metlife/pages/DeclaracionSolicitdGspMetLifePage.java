package com.qvision.metlife.pages;

import org.openqa.selenium.WebElement;

import com.qvision.metlife.utils.Esperas;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;

public class DeclaracionSolicitdGspMetLifePage extends PageObject{

Esperas espera = new Esperas();
	
	@FindBy(id = "yesDec")
	WebElement rbdFirmaTabletYes;
	
	@FindBy(id = "noDec")
	WebElement rbdFirmaTabletNo;
	
	@FindBy(id = "noEsignAgreed")
	WebElement chkReadDeclaration;
	
	@FindBy(id = "btnDeclarationContinue")
	WebElement btnContinue;
	
	public void gestioneDeclaracion() {
		espera.esperaExpEleHabilitado("noDec", 30);
		rbdFirmaTabletNo.click();
		chkReadDeclaration.click();
		btnContinue.click();
	}
}
