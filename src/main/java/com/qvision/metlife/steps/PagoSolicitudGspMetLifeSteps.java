package com.qvision.metlife.steps;

import com.qvision.metlife.pages.PagoSolicitudGspMetLifePage;

public class PagoSolicitudGspMetLifeSteps {
	
	PagoSolicitudGspMetLifePage pago = new PagoSolicitudGspMetLifePage(); 
	
	public void gestionePago(String lapsoTiempo, String formaPago, String formaPagoRecu, String devolucion) {
		pago.gestionePago(lapsoTiempo, formaPago, formaPagoRecu, devolucion);
	}
}
