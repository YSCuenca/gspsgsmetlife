package com.qvision.metlife.steps;

import com.qvision.metlife.pages.ResumenSolicitudGspMetLifePage;

public class ResumenSolicitudGspMetLifeSteps {

	ResumenSolicitudGspMetLifePage confirmar = new ResumenSolicitudGspMetLifePage();
	
	public void confirmarSolicitud() {
		confirmar.confirmarSolicitud();
	}
}
