package com.qvision.metlife.steps;

import com.qvision.metlife.pages.BeneficiariosSolicitudGspMetlifePage;

public class BeneficiariosSolicitudGspMetlifeSteps {
	
	BeneficiariosSolicitudGspMetlifePage beneficiario;
	
	public void agregarBeneficiario(String tipoBeneficiario, String tipoIdentificacion, 
			String numDocument, String firstName, String secondName, String threethName, 
			String firstSurname, String secondSurname, String relaWithAse, String fechaNacimientoT, String porcentajeP,
			String plan, String generoB, String id) {
		
		beneficiario.agregarBeneficiario(tipoBeneficiario, tipoIdentificacion, numDocument, 
				firstName, secondName, threethName, firstSurname, secondSurname, relaWithAse, fechaNacimientoT, 
				porcentajeP, plan, generoB, id);
	}
}
