package com.qvision.metlife.steps;

import com.qvision.metlife.pages.PresentarSolicitudGspMetLifePage;

public class PresentarSolicitudGspMetLifeSteps {

	PresentarSolicitudGspMetLifePage presentar = new PresentarSolicitudGspMetLifePage();
	
	public void presentarSolicitud(String pathIdenti, String pathCertificado, String pathFirma) {
		presentar.presentarSolicitud(pathIdenti, pathCertificado, pathFirma);
	}
}
