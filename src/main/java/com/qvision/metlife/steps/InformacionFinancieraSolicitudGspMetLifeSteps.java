package com.qvision.metlife.steps;

import com.qvision.metlife.pages.InformacionFinancieraSolicitudGspMetLifePage;

public class InformacionFinancieraSolicitudGspMetLifeSteps {

	InformacionFinancieraSolicitudGspMetLifePage info = new InformacionFinancieraSolicitudGspMetLifePage();
	
	public void gestioneInformacionLaboral(String tipoActividad, String nitEmpresaT, String nombreEmpresa, String empleadoMetlife,
			String actividadEconoEmp, String direccionTrabajoT, String paisTrabajoT, String deptoTrabajoT, String ciudadTrabajoT, 
			String telefonoEmpreT, String ocupacionT, String actividadEconPrinT, String ActiEconSecundariaT, String recursosPubliT, 
			String poderPublicoT, String reconocimientoPublicT, String vinculo){
		
		info.gestioneInformacionLaboral(tipoActividad, nitEmpresaT, nombreEmpresa, empleadoMetlife, actividadEconoEmp, 
				direccionTrabajoT, paisTrabajoT, deptoTrabajoT, ciudadTrabajoT, telefonoEmpreT, ocupacionT, actividadEconPrinT,
				ActiEconSecundariaT, recursosPubliT, poderPublicoT, reconocimientoPublicT, vinculo);
	}
	
	public void gestioneInformacionFinanciera(String ingresosMensualesT, String otrosIngresosT, 
			String paisOrigenIngresosT, String totalActivos, String egresosMensualesT, String totalPasivosT,
			String origenFondosT, String transaMonedaExtr, String indemSeguros) {
		
		info.gestioneInformacionFinanciera(ingresosMensualesT, otrosIngresosT, paisOrigenIngresosT, totalActivos, 
				egresosMensualesT, totalPasivosT, origenFondosT, transaMonedaExtr, indemSeguros);
	}
}
